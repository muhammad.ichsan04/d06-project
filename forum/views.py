from django.http import response
from django.shortcuts import render
from replyforum.models import ReplyForum

# Create your views here.
def forum(request):
    posts = ReplyForum.objects.all().order_by('-tanggal')

    response = {'posts' : posts}
    return render(request, "forum/forum.html", response)
