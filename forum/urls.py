from django.urls import path
from .views import forum
from replyforum.views import createpost, replyforum, editpost, deletepost, pinpost

app_name='forum'

urlpatterns = [
    path('', forum, name='forum'),
    path('create-new-post', createpost, name='createpost'),
    path('reply-a-post', replyforum, name='replyforum'),
    path('edit-a-post', editpost, name='editpost'),
    path('delete-a-post', deletepost, name='deletepost'),
    path('pin-a-post', pinpost, name='pinpost'),
]
