from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from .models import Comment
from .forms import CommentForm

# Create your views here.
def comment(request, id):
    form = CommentForm(request.POST or None)
    
    if form.is_valid():
        # save the form data to model
        instance = form.save(commit=False)
        instance.id_number = id
        instance.save()
        return HttpResponseRedirect("/recipe/" + str(id))

    response = {'form': form}
    return render(request, 'comment.html', response)
