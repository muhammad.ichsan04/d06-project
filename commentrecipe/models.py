from django.db import models

# Create your models here.

class Comment(models.Model):
    
    id_number = models.IntegerField()
    name = models.CharField(max_length=100)
    rating = models.CharField(max_length=1)
    comment = models.TextField()
