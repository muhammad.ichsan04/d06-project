from django.db import models
from django.conf import settings
from django.utils import timezone
import datetime
from django.contrib.auth.models import User
from django.db.models.deletion import DO_NOTHING

# Create your models here.
class ReplyForum(models.Model):
    tanggal = models.DateTimeField(auto_now_add=True)
    judul = models.CharField(max_length=50)
    isi = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return "{}".format(self.judul)