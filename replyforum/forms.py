from django.contrib.auth import forms
from django.forms import ModelForm, fields
from django import forms
from .models import ReplyForum 

class ReplyForumForm(forms.ModelForm):
    class Meta:
        model = ReplyForum
        fields = ['judul', 'isi']

        pesan_error = {'required':'Please Fill'}
        masukan_pertama = {'type':'text', 'placeholder':'Input Your "Title" Here'}
        masukan_kedua = {'type':'text', 'placeholder':'Input Your "Content" Here'}

        judul = forms.CharField(label='', required=True, max_length=80, widget=forms.TextInput(attrs=masukan_pertama))
        isi = forms.CharField(label='', required=True, max_length=1000, widget=forms.TextInput(attrs=masukan_kedua))

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(ReplyForumForm, self).__init__(*args, **kwargs)