from django.urls import path
from forum.views import forum

app_name='replyforum'

urlpatterns = [
    path('', forum, name='forum'),
]
