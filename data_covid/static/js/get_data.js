$.ajax({
  url: '/covid-19/ajax/get_data',
}).done(data => {
  let html_provinsi = "";
  let html_indo_global = "";
  var arrayData = Object.values(data);
  console.log(arrayData)
  for (let i = 0 ; i < arrayData[0].length; i++){
    if (i > 1 ){
      html_provinsi += `
      <div class="col d-flex justify-content-center">
        <div class="card h-100 text-center" style="width: 300px;">
          <div class="card-body">
            <h4 class="card-title">${arrayData[0][i].name}</h3>
            <h6 class="card-text">Positif</h5>
            <h6 class="card-text">${arrayData[0][i].positive}</h5>
            <h6 class="card-text">Sembuh</h5>
            <h6 class="card-text">${arrayData[0][i].recovered}</h5>
            <h6 class="card-text">Meninggal</h5>
            <h6 class="card-text">${arrayData[0][i].deaths}</h5>
          </div>
        </div>
      </div>
      `;
    } else {
      html_indo_global += `
      <div class="col d-flex justify-content-center">
        <div class="card h-100 text-center" style="width: 370px;">
          <div class="card-body">
            <h4 class="card-title">${arrayData[0][i].name}</h3>
            <h6 class="card-text">Positif</h5>
            <h6 class="card-text">${arrayData[0][i].positive}</h5>
            <h6 class="card-text">Sembuh</h5>
            <h6 class="card-text">${arrayData[0][i].recovered}</h5>
            <h6 class="card-text">Meninggal</h5>
            <h6 class="card-text">${arrayData[0][i].deaths}</h5>
          </div>
        </div>
      </div>
      `;
    }
  }
  document.getElementById("ajax-indo-global").innerHTML = html_indo_global;
  document.getElementById("ajax-provinsi").innerHTML = html_provinsi;
});
  