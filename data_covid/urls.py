from django.urls import path
from .views import data_covid, ajax_data

app_name='data_covid'

urlpatterns = [
    path('', data_covid, name='data_covid'),
    path('ajax/get_data', ajax_data, name='ajax_data'),
]