from django.urls import path
from .views import home, random, detail

app_name='recipe'

urlpatterns = [
    path('', home, name='home'),
    path('<int:id>/', detail, name='detail'),
    path('random/', random, name='random'),
]
