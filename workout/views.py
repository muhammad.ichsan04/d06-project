from django.shortcuts import render, redirect
from .forms import VideoForm
from .models import Video
from django.contrib.auth.decorators import login_required

# Create your views here.
def index(request):
  vids = Video.objects.all()
  return render(request, 'workout.html', {"vids":vids})

@login_required(login_url = '/admin/login/')
def add_video(request):
    context = {}
    form = VideoForm(request.POST or None, request.FILES or None)

    if form.is_valid():
        form.save()
        response = redirect("/workout")
        return response

    context['form'] = form
    return render(request, "add.html", context)